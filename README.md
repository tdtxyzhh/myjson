# MyJson

<p>
	<a href="https://gitee.com/tdtxyzhh/myjson/stargazers" target="_blank">
		<img src="https://svg.hamm.cn/gitee.svg?type=star&user=tdtxyzhh&project=myjson"/>
	</a>
	<a href="https://gitee.com/tdtxyzhh/myjson/members" target="_blank">
		<img src="https://svg.hamm.cn/gitee.svg?type=fork&user=tdtxyzhh&project=myjson"/>
	</a>
        <a href="https://gitee.com/tdtxyzhh/myjson/watchers" target="_blank">
		<img src="https://svg.hamm.cn/gitee.svg?type=watch&user=tdtxyzhh&project=myjson"/>
	</a>
</p>







# 1.详情介绍

已经全部完成功能！

纯C语言实现的JSON构造器与解析器，采用双向十字链表结构。

由C语言构造JSON双向十字链表结构，再向指定流输出JSON字符串；

由JSON字符串解析成C语言双向十字链表结构，再读取内容。

【版本】version 1.0

 【TDTX】
 【C99】
 【编译与运行环境】64位Windows操作系统，**TDM-gcc 4.9.2 64bit（-std=c99）编译**。
 
 【**项目Gitee仓库**】[MyJson](https://gitee.com/tdtxyzhh/myjson)，同时也放在[C语言-微项目](https://gitee.com/tdtxyzhh/c-language-microproject)。
 
 【简介】**MyJson，由C语言构造JSON字符串和由JSON字符串解析成C语言可使用数据类型，JSON构造器与JSON解析器，纯C语言实现。**

## &nbsp;&nbsp;&nbsp;&nbsp;1.1 功能
|函数|作用|
|--|--|
|**whichDataType**|判断结点类型|
|**printTheNodeInfo**|打印结点信息|

|C构造JSON函数|作用|
|--|--|
|**createInt**|创建int结点|
|**createDouble**|创建double结点|
|**createBool**|创建bool结点|
|**createNull**|创建null结点|
|**createString**|创建string结点|
|**createArray**|创建array数组结点|
|**createObject**|创建object对象结点|
|---------------------|---------------------|
|**addNodeToParent**|向对象或数组父结点挂载子结点|
|---------------------|---------------------|
|**addInt**|向对象或数组添加int结点|
|**addDouble**|向对象或数组添加double结点|
|**addBool**|向对象或数组添加bool结点|
|**addNull**|向对象或数组添加null结点|
|**addString**|向对象或数组添加string结点|
|**addArray**|向对象或数组添加iarray数组结点|
|**addObject**|向对象或数组添加iobject对象结点|
|---------------------|---------------------|
|**printMyJson**|输出为JSON字符串到输出流中|
|**printJsonString**|printMyJson的具体执行函数|
|**toJsonString**|获取JSON字符串|
|**toString**|toJsonString具体执行函数|
|**freeJson**|释放C语言构造的双向十字链表结构|

|JSON字符串解析函数|作用|
|--|--|
|**praseJsonString**|解析JSON字符串为双向十字链表|
|**prase**|praseJsonString的具体执行函数|
|**getValue**|从父节点中获取键的值|
|**getKeys**|从对象或数组父节点中获取所有子结点|

## &nbsp;&nbsp;&nbsp;&nbsp;1.2 简要测试结果

 - 解析一个单行101KB的JSON文件，执行完praseJsonString函数，花费时间约0.723-0.729秒；
 解析上述格式化后的135KB文件，执行完praseJsonString函数，花费时间约1.143秒；
 - 解析一个单行9KB的JSON文件，执行完praseJsonString函数，花费时间约0.001-0.002秒。

# 2.JSON构造器实现思路
## &nbsp;&nbsp;&nbsp;&nbsp;2.1 结点设计
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;根据JSON中值的类型，设计结点的结构如下：

```c
typedef struct MyJson
{
	//指向前一个该结构体的结点
	//如果该结点在数组中，则指向前一个元素位置结点 
	//如果该结点在对象中，则指向前一个键值对结点 
	struct MyJson* previous;
	
	//指向后一个该结构体的结点
	//如果该结点在数组中，则指向后一个元素位置结点 
	//如果该结点在对象中，则指向后一个键值对结点 
	struct MyJson* next;
	
	//指向自己的嵌套层结点,如果值是数组或对象才会使用该指针 
	struct MyJson* child;
	
	//标识键对应的值的类型（0-int,1-double,2-myBool,3-null,4-string,5-array，6-object） 
	//如果值的类型是数组或对象，则该结点内的值都为空
	char type;
	
	//指向键名称的字符指针
	//如果该结点是数组的元素，则键名为空
	const char* keyName;
	
	//存储键对应的整数类型值 
	int intValue;
	
	//存储键对应的浮点数类型值 
	double doubleValue;
	
	//存储键对应的布尔类型值（JSON字面量） 
	myBool boolValue;
	
	//存储键对应的null值（JSON字面量）
	//不用管 
	
	//存储键对应的字符串类型值 
	char* stringValue;
	
	//标记该结构体的分配场景：解析JSON字符串时候分配的true，构造JSON时候分配的false
	myBool whenMalloc;
}MyJson;
```

## &nbsp;&nbsp;&nbsp;&nbsp;2.2 数据结构设计
由于JSON中对象或数组所包含的内容都是并列的，且只有对象或数组可以容纳子数据，因此，设计数据结构为一个双向十字链表，如图：

![输入图片说明](MyJson%E5%8F%8C%E5%90%91%E5%8D%81%E5%AD%97%E9%93%BE%E8%A1%A8%E6%95%B0%E6%8D%AE%E7%BB%93%E6%9E%84.JPG)

其中，只有对象或数组结点的child指针才会指向其子数据结点，并以此完成数组嵌套数组、对象嵌套对象、数组对象互相嵌套的功能。

## &nbsp;&nbsp;&nbsp;&nbsp;2.3 构造功能函数设计

 1. 设计创造结点的函数，由于数组的中元素是单值，即可以当作没有键的值，因此可以与对象的键值结点的创建使用同一套函数，当用户传入键名的参数为NULL或空字符串时候，就说明此时创建的结点是数组中的元素。特别地，当创建对象或数组时候键名的参数为NULL或空字符串，则说明此时创建的对象为JSON根对象或数组用的元素、创建数组为JSON根数组或数组用的元素！
 2. 设计挂载结点的函数，由于只有对象或数组结点可以挂载子结点，因此在编写函数时候，要判断当前parent父结点的type值是不是5或6，如果是则执行链表尾部挂载结点操作！
 3. 设计打印输出为JSON字符串的函数，可以指定输出流为文件或标准输出，将当前已经构造好的JSON结构字符串化！

# 3.JSON解析器实现思路

 1. 设计5个令牌，分别用于定位：键起始、键结束、键与值间的冒号、值起始、值结束，其中，对于是数组中的元素，只不过前三个令牌值始终为0，总体思路就是把数组中的元素当作是对象中无键的键值对处理即可！
 2. 采用对源字符串一遍循环的方式进行解析，将对应的值调用上述构造JSON的函数创建结点和挂载结点形成与JSON字符串对应的嵌套结构。在循环过程中，如果遇到嵌套的子对象、子数组，则会进行函数递归调用，采用回溯法解析，有一个globalIndex控制着当前递归时函数解析的起始位置。



# 4. MyJson典型使用流程
## &nbsp;&nbsp;&nbsp;&nbsp;6.1 由C语言构造JSON

 	1. 创建一个JSON根对象或根数组结点，MyJson* root = createObject(NULL);
 	2. 向root结点中添加子结点，可嵌套对象、数组。
 	一是调用create系列函数，然后再调用addNodeToParent函数。
 	二是直接调用封装过的add系列函数；
 	3.调用printMyJson(stdout,root);函数向指定的输出流中打印JSON字符串；
        4.调用char* toJsonString(MyJson* root)；获取JSON字符串；
 	5.调用freeJson(root);函数释放堆内存。

## &nbsp;&nbsp;&nbsp;&nbsp;6.2 由JSON解析成C语言

	1.调用MyJson* root = praseJsonString(jsonstring);解析成C语言对应的十字链表结构；
	2.调用getKeys(root);获取当前root的全部子结点指针构成指针数组包装成Key类型结构指针返回。
	3.调用freeJson(root);函数释放堆内存。

  




## 5. 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
