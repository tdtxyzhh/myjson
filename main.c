#include "MyJson/MyJson.h"
#include "MyJson/MyJson.c"

int main(int argc, char** argv)
{
	MyJson* root = createObject(NULL);
	MyJson* array = addArray(root,"hello");
	addString(array,NULL,"ok");
	
	MyJson* object = addObject(root, "test");
	addBool(object,"hkk",myFalse);
	MyJson* array2 = addArray(object,"www");
	
	for(int i = 0;i < 10;i++)
	{
		if(i % 2 == 0)
		{
			addInt(array2,NULL,i);
		}
		else
		{
			addInt(array2,NULL,(double) (i + 1));
		}
	}
	
	MyJson* object_2 = addObject(array2,NULL);
	addString(object_2,"qqqqq","myFalse");
	
	printMyJson(stdout,root);
	
	
	Keys* root_keys = getKeys(root);
	
	for(int i = 0;i < root_keys->keysLength;i++)
	{
		
		printf("\n%s", whichDataType((root_keys->keys)[i]));
		printTheNodeInfo((root_keys->keys)[i]);
	}
	
	char* jsonString = toJsonString(root);
	
	puts(jsonString);
	
	free(jsonString);
	
	freeJson(&root);
	return 0;
}
