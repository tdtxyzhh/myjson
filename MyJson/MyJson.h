#ifndef MyJson_h
#define MyJson_h

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <float.h>

#define myFalse 0
#define myTrue  1

typedef char myBool;

//如果是解析JSON字符串时候，构造的C语言十字链表结构，则还需要释放MyJson的keyName、stringValue两个字符串指针
//如果是由C语言构造JSON时候的，则不会释放MyJson的keyName、stringValue两个字符串指针，需要用户自己管理
//调用MyJson* praseJsonString(const char* jsonString);时候先置为true，执行完prase函数后置为false 
//调用myBool freeJson(MyJson** root);时候置为false 
static myBool isPraseMalloc = myFalse;

typedef struct MyJson
{
	//指向前一个该结构体的结点
	//如果该结点在数组中，则指向前一个元素位置结点 
	//如果该结点在对象中，则指向前一个键值对结点 
	struct MyJson* previous;
	
	//指向后一个该结构体的结点
	//如果该结点在数组中，则指向后一个元素位置结点 
	//如果该结点在对象中，则指向后一个键值对结点 
	struct MyJson* next;
	
	//指向自己的嵌套层结点,如果值是数组或对象才会使用该指针 
	struct MyJson* child;
	
	//标识键对应的值的类型（0-int,1-double,2-myBool,3-null,4-string,5-array，6-object） 
	//如果值的类型是数组或对象，则该结点内的值都为空
	char type;
	
	//指向键名称的字符指针
	//如果该结点是数组的元素，则键名为空
	const char* keyName;
	
	//存储键对应的整数类型值 
	int intValue;
	
	//存储键对应的浮点数类型值 
	double doubleValue;
	
	//存储键对应的布尔类型值（JSON字面量） 
	myBool boolValue;
	
	//存储键对应的null值（JSON字面量）
	//不用管 
	
	//存储键对应的字符串类型值 
	char* stringValue;
	
	//标记该结构体的分配场景：解析JSON字符串时候分配的true，构造JSON时候分配的false
	myBool whenMalloc;
}MyJson;

typedef struct Keys
{
	//指示keys一维数组的元素个数 
	int keysLength;
	
	//指向对象中键值对结点的所有指针构成的指针数组 
	MyJson** keys;  
}Keys;


const char* whichDataType(MyJson* theNode);                           //判断该结点是那种类型的值，直接返回其字符串名称 

void printTheNodeInfo(MyJson* theNode);                               //打印该结点信息 


/*****************************************由C语言创建JSON数据格式结构***************************************/
MyJson* createInt(const char* keyString,int newInt);                  //创建int键值对 

MyJson* createDouble(const char* keyString,double newDouble);         //创建double键值对 

MyJson* createBool(const char* keyString,myBool newBool);             //创建bool键值对 

MyJson* createNull(const char* keyString);                            //创建NULL键值对 

MyJson* createString(const char* keyString,const char* newString);    //创建string键值对

MyJson* createArray(const char* keyString);                           //创建array键值对

MyJson* createObject(const char* keyString);                          //创建object键值对


myBool addNodeToParent(MyJson* parent,MyJson* thNode);                //向父结点位置挂载已有结点,父结点只有是array或object才可以挂载子结点 



myBool addInt(MyJson* parent,const char* keyString,int newValue);                 //向父结点位置插入int结点 

myBool addDouble(MyJson* parent,const char* keyString,double newValue);           //向父结点位置插入子double结点 

myBool addBool(MyJson* parent,const char* keyString,myBool newValue);             //向父结点位置插入子bool结点

myBool addNull(MyJson* parent,const char* keyString);                             //向父结点位置插入子null结点

myBool addString(MyJson* parent,const char* keyString,const char* newString);     //向父结点位置插入子string结点
 
MyJson* addArray(MyJson* parent,const char* keyString);                           //向父结点位置插入子array结点

MyJson* addObject(MyJson* parent,const char* keyString);                           //向父结点位置插入子object结点


myBool printMyJson(FILE* printOut,MyJson* root);                              //向指定的输出流中写入JSON字符串 

void printJsonString(FILE* printOut,MyJson* root);                            //打印JSON字符串

char* toJsonString(MyJson* root);                                              //获取JSON字符串char* 

void toString(MyJson* root, char** jsonStringPointer, int* jsonStringSpaceLengthPointer);    //获取JSON字符串char* 

myBool freeJson(MyJson** root);                                                //释放C语言JSON链表结构堆内存        
/*****************************************由C语言创建JSON数据格式结构***************************************/


/************************************由JSON字符串结构解析成C语言可用数据类型**********************************/
MyJson* praseJsonString(const char* jsonString);                           //将JSON字符串解析为双向十字链表结构

MyJson* prase(MyJson* parent,const char* json,const char* keyString,int* index);      //具体解析函数 

MyJson* getValue(MyJson* parent,const char* keyString);                   //从给定的JSON中得到key结点指针返回

Keys* getKeys(MyJson* parent);                                            //从给定的JSON父结点中返回所有key结点指针 
/************************************由JSON字符串结构解析成C语言可用数据类型**********************************/


/*****************************************辅助函数*******************************************************/

myBool getStringOtherSpace(char** jsonStringPointer, int* jsonStringSpaceLengthPointer, int toUsingSpaceLength);

myBool strcatString(char** jsonStringPointer, int* jsonStringSpaceLengthPointer, char* keyValueString,int totalLength);

/*****************************************辅助函数*******************************************************/

#ifdef __cplusplus
}
#endif

#endif
