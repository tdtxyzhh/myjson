#include "MyJson.h"
                  
//判断该结点是那种类型的值，直接返回其字符串名称             
const char* whichDataType(MyJson* theNode)
{
	if(!theNode)
	{
		return "unknow";
	}
	switch(theNode->type)
	{
		case 0:
			return "int";
		case 1:
			return "double";
		case 2:
			return "bool";
		case 3:
			return "null";
		case 4:
			return "string";
		case 5:
			return "array";
		case 6:
			return "object";
		default:
			return "unknow"; 
	}
}

//打印该结点信息
void printTheNodeInfo(MyJson* theNode)
{
	if(!theNode)
	{
		puts("\n空指针，不可打印！");
		return;
	}
	printf("\ntheNodeAddress:%#X",theNode);
	
	if(!(theNode->keyName))
	{
		printf("\ntheNode->keyName:NULL空键名");	
	}
	else
	{
		printf("\ntheNode->keyName:\"%s\"",theNode->keyName);
	}
	
	if(!(theNode->previous))
	{
		printf("\ntheNode->previous:NULL");	
	}
	else
	{
		printf("\ntheNode->previous:%#X",theNode->previous);	
	}
	
	if(!(theNode->next))
	{
		printf("\ntheNode->next:NULL");	
	}
	else
	{
		printf("\ntheNode->next:%#X",theNode->next);	
	}
	
	if(!(theNode->child))
	{
		printf("\ntheNode->child:NULL");	
	}
	else
	{
		printf("\ntheNode->child:%#X",theNode->child);	
	}

	switch(theNode->type)
	{
		case 0:
			printf("\ntheNode->type:%d-%s",0,"int");
			printf("\ntheNode->intValue:%d\n",theNode->intValue);
			break;
		case 1:
			printf("\ntheNode->type:%d-%s",1,"double");
			printf("\ntheNode->doubleValue:%f\n",theNode->doubleValue);
			break;
		case 2:
			printf("\ntheNode->type:%d-%s",2,"bool");
			printf("\ntheNode->boolValue:%s\n",!theNode->boolValue ? "false" : "true");
			break;
		case 3:
			printf("\ntheNode->type:%d-%s",3,"null");
			printf("\ntheNode->nullValue:%s\n","null");
			break;
		case 4:
			printf("\ntheNode->type:%d-%s",4,"string");
			printf("\ntheNode->stringValue:\"%s\"\n",theNode->stringValue);
			break;
		case 5:
			printf("\ntheNode->type:%d-%s",5,"array");
			if(!(theNode->child))
			{
				printf("\ntheNode->arrayChildAddress:NULL\n");
			}
			else
			{
				printf("\ntheNode->arrayChildAddress:%#X\n",theNode->child);
			}
			break;
		case 6:
			printf("\ntheNode->type:%d-%s",6,!theNode->keyName ? "rootObject" : "object");
			if(!(theNode->child))
			{
				printf("\ntheNode->objectChildAddress:NULL\n");
			}
			else
			{
				printf("\ntheNode->objectChildAddress:%#X\n",theNode->child);
			}
			break;
		default:
			break; 
	}
}                                                                    

/***************************************由C语言创建JSON数据格式结构***************************************/

//创建int键值对 
MyJson* createInt(const char* keyString,int newInt)
{
	MyJson* theInt = (MyJson*) malloc(sizeof(MyJson));
	
	if(!theInt)
	{
		puts("\n[Error]创建结点错误：创建新结点时分配空间失败！");
		return NULL;
	}
	
	//前后结点、子结点暂时为空 
	theInt->previous = NULL;
	theInt->next = NULL;
	theInt->child = NULL;
	theInt->type = 0;
	
	//如果keyString是NULL，则说明此处创建的是一个数组用的单值，否则为一般性键值对
	if(!keyString)
	{
		theInt->keyName = NULL;
	}
	else if(!strlen(keyString))
	{
		theInt->keyName = NULL;		
	}
	else
	{
		theInt->keyName = keyString;	
	}
	
	theInt->intValue = newInt;
	theInt->doubleValue = DBL_MIN;
	theInt->boolValue = myFalse;
	theInt->stringValue = NULL;
	
	theInt->whenMalloc = isPraseMalloc;
	
	return theInt;
}              

//创建double键值对 
MyJson* createDouble(const char* keyString,double newDouble)
{
	MyJson* theDouble = (MyJson*) malloc(sizeof(MyJson));
	
	if(!theDouble)
	{
		puts("\n[Error]创建结点错误：创建新结点时分配空间失败！");
		return NULL;
	}
	
	//前后结点、子结点暂时为空 
	theDouble->previous = NULL;
	theDouble->next = NULL;
	theDouble->child = NULL;
	theDouble->type = 1;
	
	//如果keyString是NULL，则说明此处创建的是一个数组用的单值，否则为一般性键值对
	if(!keyString)
	{
		theDouble->keyName = NULL;
	}
	else if(!strlen(keyString))
	{
		theDouble->keyName = NULL;		
	}
	else
	{
		theDouble->keyName = keyString;	
	}
	
	theDouble->intValue = INT_MIN;
	theDouble->doubleValue = newDouble;
	theDouble->boolValue = myFalse;
	theDouble->stringValue = NULL;
	
	theDouble->whenMalloc = isPraseMalloc;
	
	return theDouble;	
}    

//创建bool键值对 
MyJson* createBool(const char* keyString,myBool newBool)
{
	MyJson* theBool = (MyJson*) malloc(sizeof(MyJson));
	
	if(!theBool)
	{
		puts("\n[Error]创建结点错误：创建新结点时分配空间失败！");
		return NULL;
	}
	
	//前后结点、子结点暂时为空 
	theBool->previous = NULL;
	theBool->next = NULL;
	theBool->child = NULL;
	theBool->type = 2;
	
	//如果keyString是NULL，则说明此处创建的是一个数组用的单值，否则为一般性键值对
	if(!keyString)
	{
		theBool->keyName = NULL;
	}
	else if(!strlen(keyString))
	{
		theBool->keyName = NULL;		
	}
	else
	{
		theBool->keyName = keyString;	
	}
	
	theBool->intValue = INT_MIN;
	theBool->doubleValue = DBL_MIN;
	theBool->boolValue = newBool;
	theBool->stringValue = NULL;
	
	theBool->whenMalloc = isPraseMalloc;
	
	return theBool;		
}             

//创建NULL键值对
MyJson* createNull(const char* keyString)
{
	MyJson* theNull = (MyJson*) malloc(sizeof(MyJson));
	
	if(!theNull)
	{
		puts("\n[Error]创建结点错误：创建新结点时分配空间失败！");
		return NULL;
	}
	
	//前后结点、子结点暂时为空 
	theNull->previous = NULL;
	theNull->next = NULL;
	theNull->child = NULL;
	theNull->type = 3;
	
	//如果keyString是NULL，则说明此处创建的是一个数组用的单值，否则为一般性键值对
	if(!keyString)
	{
		theNull->keyName = NULL;
	}
	else if(!strlen(keyString))
	{
		theNull->keyName = NULL;		
	}
	else
	{
		theNull->keyName = keyString;	
	}
	
	theNull->intValue = INT_MIN;
	theNull->doubleValue = DBL_MIN;
	theNull->boolValue = myFalse;
	theNull->stringValue = NULL;
	
	theNull->whenMalloc = isPraseMalloc;
	
	return theNull;		
}                  

//创建string键值对
MyJson* createString(const char* keyString,const char* newString)
{
	MyJson* theString = (MyJson*) malloc(sizeof(MyJson));
	
	if(!theString)
	{
		puts("\n[Error]创建结点错误：创建新结点时分配空间失败！");
		return NULL;
	}
	
	//前后结点、子结点暂时为空 
	theString->previous = NULL;
	theString->next = NULL;
	theString->child = NULL;
	theString->type = 4;
	
	//如果keyString是NULL，则说明此处创建的是一个数组用的单值，否则为一般性键值对
	if(!keyString)
	{
		theString->keyName = NULL;
	}
	else if(!strlen(keyString))
	{
		theString->keyName = NULL;		
	}
	else
	{
		theString->keyName = keyString;	
	}
	
	theString->intValue = INT_MIN;
	theString->doubleValue = DBL_MIN;
	theString->boolValue = myFalse;
	theString->stringValue = (char*) newString;
	
	theString->whenMalloc = isPraseMalloc;
	
	return theString;		
}       

//创建array键值对
MyJson* createArray(const char* keyString)
{
	MyJson* array = (MyJson*) malloc(sizeof(MyJson));
	if(!array)
	{
		puts("\n[Error]创建结点错误：创建新结点时分配空间失败！");
		return NULL;
	}
	//前后结点、子结点暂时为空 
	array->previous = NULL;
	array->next = NULL;
	array->child = NULL;
	array->type = 5;
	
	//如果keyString是NULL，则说明此处创建的是JSON数组用的元素（是个数组），否则为一般性键值对
	if(!keyString)
	{
		array->keyName = NULL;
	}
	else if(!strlen(keyString))
	{
		array->keyName = NULL;		
	}
	else
	{
		array->keyName = keyString;	
	}
	
	array->intValue = INT_MIN;
	array->doubleValue = DBL_MIN;
	array->boolValue = myFalse;
	array->stringValue = NULL;
	
	array->whenMalloc = isPraseMalloc;
	
	return array;	
}               

//创建object键值对
MyJson* createObject(const char* keyString)
{
	MyJson* object = (MyJson*) malloc(sizeof(MyJson));
	if(!object)
	{
		puts("\n[Error]创建结点错误：创建新结点时分配空间失败！");
		return NULL;
	}
	//前后结点、子结点暂时为空 
	object->previous = NULL;
	object->next = NULL;
	object->child = NULL;
	object->type = 6;
	
	//如果keyString是NULL，则说明此处创建的是JSON整个结构的根对象结点或数组元素对象结点，否则为一般性对象结点
	if(!keyString)
	{
		object->keyName = NULL;
	}
	else if(!strlen(keyString))
	{
		object->keyName = NULL;		
	}
	else
	{
		object->keyName = keyString;	
	}
	
	object->intValue = INT_MIN;
	object->doubleValue = DBL_MIN;
	object->boolValue = myFalse;
	object->stringValue = NULL;
	
	object->whenMalloc = isPraseMalloc;
	
	return object;
}                            


//向父结点位置挂载已有结点 
myBool addNodeToParent(MyJson* parent,MyJson* theNode)
{
	if(!parent || !theNode)
	{
		//父节点指针和需要挂载结点的指针不为空才执行函数 
		puts("\n[Error]挂载结点错误：父结点指针或需挂载结点指针为空!"); 
		return myFalse;
	}
	int parentType = parent->type;
	if(parentType != 5 && parentType != 6)
	{
		//只有父节点是数组或对象才可挂载子结点 
		puts("\n[Error]挂载结点错误：父结点不是数组或对象不可挂载子结点!");
		return myFalse;
	}
	if(parentType == 5 && theNode->keyName)
	{
		//如果父结点是数组，但需要挂载结点不是单值而是键值对，则不可挂载
		puts("\n[Error]挂载结点错误：数组型父节点不可挂载键值对,只能挂载单值!");
		return myFalse;
	}
	if(parentType == 6 && !theNode->keyName)
	{
		//如果父结点是对象，但需要挂载结点是单值而不是键值对，则不可挂载
		puts("\n[Error]挂载结点错误：对象型父节点不可挂载单值,只能挂载键值对!");
		return myFalse;
	}
	if(parent == theNode)
	{
		//相同指针的不可自我挂载，防止free时出错，请调用create函数重新生成结点后再挂载！
		puts("\n[Error]挂载结点错误：相同指针不能自我挂载!"); 
		return myFalse;
	}
	
	if(!parent->child)
	{
		//如果父节点结构当前没有任何一个子结点，对于对象来说不存在相同键名问题、对于数组更不涉及该问题，则直接挂载到child指针上
		//并将子结点的前驱指针置NULL、后继指针置NULL 
		parent->child = theNode;
		theNode->previous = NULL;
		theNode->next = NULL; 
		return myTrue;	
	}
	
	//如果此时已经有了子结点，则将该结点挂载为最后一个子结点的后继结点
	//并完成双向指针指向操作
	
	MyJson* t = parent->child;
	myBool isInObject = myFalse;
	myBool isSamePoint = myFalse;
	if(parentType == 5)
	{
		//如果父结点是数组，则任意挂载即可，由于不存在键，不用检验键的唯一性问题
		while(1)
		{
			if(t == theNode)
			{
				isSamePoint = myTrue;
				break;
			}
			if(t->next)
			{
				t = t->next;
			}
			else
			{
				break;
			}
		}
	}
	else
	{
		//如果父节点是对象，需要检验需要挂载的键是否已经存在
		while(1)
		{
			if(t == theNode)
			{
				isSamePoint = myTrue;
				break;
			}
			if(!strcmp(t->keyName,theNode->keyName)) 
			{
				isInObject = myTrue;
				break;
			}
			if(t->next)
			{
				t = t->next;
			}
			else
			{
				break;
			}
		}
	}

	
	//经过循环已经找到当前父结点结构的最后一个子结点t，开始挂载操作
	//如果父结点是数组型，则不会触发检验键唯一性分支，即isInObject总是为false;
	//如果父结点是对象型，需挂载结点的键不存在于父结点中，则isInObject是false，如果存在，则isInObject是true; 
	//因此，直接判断isInObject是false时就可以直接挂载，是true不可挂载！ 
	if(!isInObject && !isSamePoint)
	{
		t->next = theNode; //将t的next指针指向theNode需要挂载的结点 
		theNode->previous = t; //将被挂载结点的previous指针指向当前父结点结构最后一个子结点t
		
		theNode->next = NULL; //此时theNode已经被挂载上成为最后一个子结点，将其next指针置为空
		return myTrue;	
	}
	else
	{
		if(isSamePoint)
		{
			//需挂载结点的指针已经被挂载到对象型父结点中
			puts("\n[Error]挂载结点错误：需挂载结点的指针已经挂载，不可重复挂载!"); 
		}
		if(isInObject)
		{
			//需挂载结点的键已经存在于对象型父结点中
			puts("\n[Error]挂载结点错误：需挂载结点的键已经存在于对象型父结点中，不可重复挂载!"); 
		}
		return myFalse; 
	}

	return myFalse; 
}             

//向父结点位置插入int结点 
myBool addInt(MyJson* parent,const char* keyString,int newValue)
{
	MyJson* newNode = createInt(keyString,newValue);
	if(!newNode)
	{
		return myFalse;
	}

	return addNodeToParent(parent,newNode);	
}                 

//向父结点位置插入子double结点
myBool addDouble(MyJson* parent,const char* keyString,double newValue)           
{
	MyJson* newNode = createDouble(keyString,newValue);
	if(!newNode)
	{
		return myFalse;
	}

	return addNodeToParent(parent,newNode);	
}

//向父结点位置插入子bool结点
myBool addBool(MyJson* parent,const char* keyString,myBool newValue)           
{
	MyJson* newNode = createBool(keyString,newValue);
	if(!newNode)
	{
		return myFalse;
	}

	return addNodeToParent(parent,newNode);		
}

//向父结点位置插入子null结点
myBool addNull(MyJson* parent,const char* keyString)                            
{
	MyJson* newNode = createNull(keyString);
	if(!newNode)
	{
		return myFalse;
	}

	return addNodeToParent(parent,newNode);		
}

//向父结点位置插入子string结点
myBool addString(MyJson* parent,const char* keyString,const char* newString)
{
	MyJson* newNode = createString(keyString,newString);
	if(!newNode)
	{
		return myFalse;
	}

	return addNodeToParent(parent,newNode);		
}  

//向父结点位置插入子array结点
MyJson* addArray(MyJson* parent,const char* keyString)            
{
	MyJson* newNode = createArray(keyString);
	if(!newNode)
	{
		return myFalse;
	}

	myBool t = addNodeToParent(parent,newNode);
	return t ? newNode : NULL;		
}

//向父结点位置插入子object结点
MyJson* addObject(MyJson* parent,const char* keyString)                                        
{
	MyJson* newNode = createObject(keyString);
	if(!newNode)
	{
		return myFalse;
	}

	myBool t = addNodeToParent(parent,newNode);
	return t ? newNode : NULL;		
}

//向指定的输出流中写入JSON字符串 
myBool printMyJson(FILE* printOut,MyJson* root)
{
	if(!root)
	{
		puts("\n[Error]输出JSON字符串错误：JSON根对象结点指针为NULL！");
		return myFalse;
	}
	
	/*
	if(fileRoot->type != 6)
	{
		return myFalse;
	}
	*/
	
	if(!printOut)
	{
		puts("\n[Error]输出JSON字符串错误：输出流指针为NULL,请指定正确的输出对象！");
		return myFalse;
	}
	
	//puts("\n\n===>[START]开始将C语言JSON结果输出为JSON字符串:\n"); 
	fflush(printOut);
	
	printJsonString(printOut,root);
	
	//puts("\n===>[E N D]输出为JSON字符串完成！"); 
	fflush(printOut);
	
	return myFalse;	
}

//打印JSON字符串
void printJsonString(FILE* printOut,MyJson* root)
{
	if(!root)
	{
		return;
	}
	/*
	if(fileRoot->type != 6)
	{
		return false;
	}
	*/
	if(!printOut)
	{
		return;
	}
	
	int type = root->type;
	switch(type)
	{
		case 0:
			if(root->keyName)
			{
				if(!root->next)
				{
					fprintf(printOut,"\"%s\":%d",root->keyName,root->intValue);
				}
				else
				{
					fprintf(printOut,"\"%s\":%d,",root->keyName,root->intValue);
				}
			}
			else
			{
				if(!root->next)
				{
					fprintf(printOut,"%d",root->intValue);
				}
				else
				{
					fprintf(printOut,"%d,",root->intValue);
				}
			}
			printJsonString(printOut,root->child);
			break;
		case 1:
			if(root->keyName)
			{
				if(!root->next)
				{
					fprintf(printOut,"\"%s\":%f",root->keyName,root->doubleValue);
				}
				else
				{
					fprintf(printOut,"\"%s\":%f,",root->keyName,root->doubleValue);
				}				
			}
			else
			{
				if(!root->next)
				{
					fprintf(printOut,"%f",root->doubleValue);
				}
				else
				{
					fprintf(printOut,"%f,",root->doubleValue);
				}
				
			}
			printJsonString(printOut,root->child);
			break;
		case 2:
			if(root->keyName)
			{
				if(!root->next)
				{
					fprintf(printOut,"\"%s\":%s",root->keyName,root->boolValue == myFalse ? "false" : "true");
				}
				else
				{
					fprintf(printOut,"\"%s\":%s,",root->keyName,root->boolValue == myFalse ? "false" : "true");
				}	
			}
			else
			{
				if(!root->next)
				{
					fprintf(printOut,"%s",root->boolValue == myFalse ? "false" : "true");
				}
				else
				{
					fprintf(printOut,"%s,",root->boolValue == myFalse ? "false" : "true");
				}
				
			}
			printJsonString(printOut,root->child);
			break;
		case 3:
			if(root->keyName)
			{
				if(!root->next)
				{
					fprintf(printOut,"\"%s\":%s",root->keyName,"null");
				}
				else
				{
					fprintf(printOut,"\"%s\":%s,",root->keyName,"null");
				}	
			}
			else
			{
				if(!root->next)
				{
					fprintf(printOut,"%s","null");
				}
				else
				{
					fprintf(printOut,"%s,","null");
				}
			}
			printJsonString(printOut,root->child);
			break;
		case 4:
			if(root->keyName)
			{
				if(!root->next)
				{
					fprintf(printOut,"\"%s\":\"%s\"",root->keyName,root->stringValue);
				}
				else
				{
					fprintf(printOut,"\"%s\":\"%s\",",root->keyName,root->stringValue);
				}
			}
			else
			{
				if(!root->next)
				{
					fprintf(printOut,"\"%s\"",root->stringValue);
				}
				else
				{
					fprintf(printOut,"\"%s\",",root->stringValue);
				}
			}
			printJsonString(printOut,root->child);
			break;
		case 5:
			if(root->keyName)
			{
				fprintf(printOut,"\"%s\":[",root->keyName);
				printJsonString(printOut,root->child);
				if(!root->next)
				{
					fprintf(printOut,"]");
				}
				else
				{
					fprintf(printOut,"],");
				}
			}
			else
			{
				fprintf(printOut,"[");
				printJsonString(printOut,root->child);
				if(!root->next)
				{
					fprintf(printOut,"]");
				}
				else
				{
					fprintf(printOut,"],");
				}
			}
			break;
		case 6:
			if(root->keyName)
			{
				fprintf(printOut,"\"%s\":{",root->keyName);
				printJsonString(printOut,root->child);
				if(!root->next)
				{
					fprintf(printOut,"}");
				}
				else
				{
					fprintf(printOut,"},");
				}
			}
			else
			{
				fprintf(printOut,"{");
				printJsonString(printOut,root->child);
				if(!root->next)
				{
					fprintf(printOut,"}");
				}
				else
				{
					fprintf(printOut,"},");
				}
			}
			break;
		default:
			break;
	}
	if(!root->next)
	{
		return;
	}
	else
	{
		printJsonString(printOut,root->next);
	}
	return; 
} 

//获取JSON字符串char*
char* toJsonString(MyJson* root)
{
	if(!root)
	{
		puts("\n[Error]输出JSON字符串错误：JSON根对象结点指针为NULL！");
		return myFalse;
	}
	
	int jsonStringSpaceLength = 128;

	char* jsonString = (char*)malloc(sizeof(char) * jsonStringSpaceLength);
	jsonString[0] = '\0';
	
	if(!jsonString)
	{
		puts("\n[Error]输出JSON字符串错误：结果字符串空间malloc失败！");
		return NULL;
	}

	
	//puts("\n\n===>[START]开始将C语言JSON结果输出为JSON字符串:\n"); 

	toString(root, &jsonString,&jsonStringSpaceLength);
	
	
	//puts("\n===>[E N D]输出为JSON字符串完成！"); 

	
	return jsonString;
}

//获取JSON字符串char* 
void toString(MyJson* root, char** jsonStringPointer, int* jsonStringSpaceLengthPointer)
{
	if(!root)
	{
		return;
	}
	
	int type = root->type;
	switch(type)
	{
		case 0:
			if(root->keyName)
			{
				int totalLength = strlen(root->keyName) + 17;
				char keyValueString[totalLength];
				keyValueString[0] = '\0';
				if(!root->next)
				{
					sprintf(keyValueString,"\"%s\":%d",root->keyName,root->intValue);
				}
				else
				{
					sprintf(keyValueString,"\"%s\":%d,",root->keyName,root->intValue);
				}
				strcatString(jsonStringPointer, jsonStringSpaceLengthPointer, keyValueString, totalLength);
			}
			else
			{
				char keyValueString[13];
				keyValueString[0] = '\0';
				if(!root->next)
				{
					sprintf(keyValueString,"%d",root->intValue);
				}
				else
				{
					sprintf(keyValueString,"%d,",root->intValue);
				}
				strcatString(jsonStringPointer, jsonStringSpaceLengthPointer, keyValueString, 13);
			}
			toString(root->child, jsonStringPointer, jsonStringSpaceLengthPointer);
			break;
		case 1:
			if(root->keyName)
			{
				int totalLength = strlen(root->keyName) + 515;
				char keyValueString[totalLength];
				keyValueString[0] = '\0';
				if(!root->next)
				{
					sprintf(keyValueString,"\"%s\":%d",root->keyName,root->intValue);
				}
				else
				{
					sprintf(keyValueString,"\"%s\":%f,",root->keyName,root->doubleValue);
				}
				strcatString(jsonStringPointer, jsonStringSpaceLengthPointer, keyValueString, totalLength);			
			}
			else
			{
				char keyValueString[515];
				keyValueString[0] = '\0';
				if(!root->next)
				{
					sprintf(keyValueString,"%f",root->doubleValue);
				}
				else
				{
					sprintf(keyValueString,"%f,",root->doubleValue);
				}
				strcatString(jsonStringPointer, jsonStringSpaceLengthPointer, keyValueString, 515);
			}
			toString(root->child, jsonStringPointer, jsonStringSpaceLengthPointer);
			break;
		case 2:
			if(root->keyName)
			{
				int totalLength = strlen(root->keyName) + 10;
				char keyValueString[totalLength];
				keyValueString[0] = '\0';
				if(!root->next)
				{
					sprintf(keyValueString,"\"%s\":%s",root->keyName,root->boolValue == myFalse ? "false" : "true");
				}
				else
				{
					sprintf(keyValueString,"\"%s\":%s,",root->keyName,root->boolValue == myFalse ? "false" : "true");
				}	
				strcatString(jsonStringPointer, jsonStringSpaceLengthPointer, keyValueString, totalLength);
			}
			else
			{
				char keyValueString[6];
				keyValueString[0] = '\0';
				if(!root->next)
				{
					sprintf(keyValueString,"%s",root->boolValue == myFalse ? "false" : "true");
				}
				else
				{
					sprintf(keyValueString,"%s,",root->boolValue == myFalse ? "false" : "true");
				}
				strcatString(jsonStringPointer, jsonStringSpaceLengthPointer, keyValueString, 6);
			}
			toString(root->child, jsonStringPointer, jsonStringSpaceLengthPointer);
			break;
		case 3:
			if(root->keyName)
			{
				int totalLength = strlen(root->keyName) + 9;
				char keyValueString[totalLength];
				keyValueString[0] = '\0';
				if(!root->next)
				{
					sprintf(keyValueString,"\"%s\":%s",root->keyName,"null");
				}
				else
				{
					sprintf(keyValueString,"\"%s\":%s,",root->keyName,"null");
				}
				strcatString(jsonStringPointer, jsonStringSpaceLengthPointer, keyValueString, totalLength);
			}
			else
			{
				char keyValueString[5];
				keyValueString[0] = '\0';
				if(!root->next)
				{
					sprintf(keyValueString,"%s","null");
				}
				else
				{
					sprintf(keyValueString,"%s,","null");
				}
				strcatString(jsonStringPointer, jsonStringSpaceLengthPointer, keyValueString, 5);
			}
			toString(root->child, jsonStringPointer, jsonStringSpaceLengthPointer);
			break;
		case 4:
			if(root->keyName)
			{
				int totalLength = strlen(root->keyName) + strlen(root->stringValue) + 6;
				char keyValueString[totalLength];
				keyValueString[0] = '\0';
				if(!root->next)
				{
					sprintf(keyValueString,"\"%s\":\"%s\"",root->keyName,root->stringValue);
				}
				else
				{
					sprintf(keyValueString,"\"%s\":\"%s\",",root->keyName,root->stringValue);
				}
				strcatString(jsonStringPointer, jsonStringSpaceLengthPointer, keyValueString, totalLength);
			}
			else
			{
				int totalLength = strlen(root->stringValue) + 3;
				char keyValueString[totalLength];
				keyValueString[0] = '\0';
				if(!root->next)
				{
					sprintf(keyValueString,"\"%s\"",root->stringValue);
				}
				else
				{
					sprintf(keyValueString,"\"%s\",",root->stringValue);
				}
				strcatString(jsonStringPointer, jsonStringSpaceLengthPointer, keyValueString, totalLength);
			}
			toString(root->child, jsonStringPointer, jsonStringSpaceLengthPointer);
			break;
		case 5:
			if(root->keyName)
			{
				int totalLength = strlen(root->keyName) + 5;
				char keyValueString[totalLength];
				keyValueString[0] = '\0';
				
				sprintf(keyValueString,"\"%s\":[",root->keyName);
				strcatString(jsonStringPointer, jsonStringSpaceLengthPointer, keyValueString, totalLength);
				
				toString(root->child, jsonStringPointer, jsonStringSpaceLengthPointer);
				if(!root->next)
				{
					strcatString(jsonStringPointer, jsonStringSpaceLengthPointer, "]", 2);
				}
				else
				{
					strcatString(jsonStringPointer, jsonStringSpaceLengthPointer, "],", 3);
				}
			}
			else
			{
				strcatString(jsonStringPointer, jsonStringSpaceLengthPointer, "[", 2);

				toString(root->child, jsonStringPointer, jsonStringSpaceLengthPointer);
				if(!root->next)
				{
					strcatString(jsonStringPointer, jsonStringSpaceLengthPointer, "]", 2);
				}
				else
				{
					strcatString(jsonStringPointer, jsonStringSpaceLengthPointer, "]", 3);
				}
			}
			break;
		case 6:
			if(root->keyName)
			{
				int totalLength = strlen(root->keyName) + 5;
				char keyValueString[totalLength];
				keyValueString[0] = '\0';
				
				sprintf(keyValueString,"\"%s\":{",root->keyName);
				strcatString(jsonStringPointer, jsonStringSpaceLengthPointer, keyValueString, totalLength);
				
				toString(root->child, jsonStringPointer, jsonStringSpaceLengthPointer);
				if(!root->next)
				{
					strcatString(jsonStringPointer, jsonStringSpaceLengthPointer, "}", 2);
				}
				else
				{
					strcatString(jsonStringPointer, jsonStringSpaceLengthPointer, "},", 3);
				}
			}
			else
			{
				strcatString(jsonStringPointer, jsonStringSpaceLengthPointer, "{", 2);
				toString(root->child, jsonStringPointer, jsonStringSpaceLengthPointer);
				if(!root->next)
				{
					strcatString(jsonStringPointer, jsonStringSpaceLengthPointer, "}", 2);
				}
				else
				{
					strcatString(jsonStringPointer, jsonStringSpaceLengthPointer, "},", 3);
				}
			}
			break;
		default:
			break;
	}
	if(!root->next)
	{
		return;
	}
	else
	{
		toString(root->next, jsonStringPointer, jsonStringSpaceLengthPointer);
	}
	return;
}

//释放C语言JSON链表结构堆内存
myBool freeJson(MyJson** root)
{
	//回溯法-递归实现释放 
	//采用递归释放，先释放当前节点，保存next指针和child指针
	//然后释放child指针，最后进释放next结点
	if(!root || !(*root))
	{
		//puts("释放结点指针为空！");
		return myFalse;
	}
	
	MyJson* child = (*root)->child;
	MyJson* next = (*root)->next;
	
	(*root)->child = NULL;
	(*root)->next = NULL;
	
	if((*root)->whenMalloc)
	{
		free((void*)(*root)->keyName);
		free((*root)->stringValue);
		(*root)->keyName = NULL;
		(*root)->stringValue = NULL;
	}
	
	free(*root); //释放当前结点
	*root = NULL;
	
	freeJson(&child); //释放子节点 
	
	freeJson(&next); //释放next结点 
	
	
	return myTrue;	
}                                                                    
/***************************************由C语言创建JSON数据格式结构***************************************/


/********************************由JSON字符串结构解析成C语言可用数据类型**********************************/                                
//将JSON字符串解析为十字链表结构
MyJson* praseJsonString(const char* jsonString)
{
	int globalIndex = 0;
	isPraseMalloc = myTrue;
	MyJson* result = prase(NULL,jsonString,NULL,&globalIndex);
	isPraseMalloc = myFalse;
	return result;
}                      

//具体解析函数 
MyJson* prase(MyJson* parent,const char* json,const char* keyString,int* index)
{
	if(!json)
	{
		//待解析的字符串指针为空，则不执行 
		return NULL;
	}
	
	//puts("\n检查待解析JSON字符串：");
	//puts(&json[*index]);
	
	if(!parent && keyString)
	{
		//父结点为空，keyString是NULL，说明此时是刚开始解析JSON字符串，准备解析JSON根对象 
		return NULL;
	}
	
	if(json[*index] == '{'  && json[strlen(json) - 1] != '}' && *index == 0)
	{
		//如果首字符是{，但尾字符不是}，则JSON语法结构一定错误，则不执行 
		//但是不能去判断[]的成对出现 
		return NULL;
	}
	if(json[*index] == '[' && (json[strlen(json) - 1] != '}' && json[strlen(json) - 1] != ']'))
	{
		//如果首字符是[,但尾字符不是}或]，则JSON语法结构一定错误，则不执行 
		return NULL;
	}
	
	if(!parent && !keyString && ((json[*index] != '{' && json[strlen(json) - 1] != '}') && (json[*index] != '[' && json[strlen(json) - 1] != ']')))
	{
		//如果父结点是空，则说明此时解析的JSON文件根对象 
		//对于JSON文件根对象，必要要求首字符是{且尾字符是}  或  首字符是[且尾字符是]
		return NULL;
	}
	
	//	0		1		2			3		4		5                        6
	//	键开始  键结束  键值冒号    值开始  值结束  键值对分割点（，或}）    值类型
	//上面是5个令牌 
	//值类型：0-int,1-double,2-bool,3-null,4-string,5-array,6-object 
	//loc初始化值：loc[0] = 0,loc[1] = 0,loc[2] = 0,loc[3] = 0,loc[4] = 0,loc[5] = 0,loc[6] = -1 
	int loc[7] = {0};
	loc[6] = -1;
	//char* objectKeyString = NULL;
	
	//当值是0、1、2、3、4类型时 
	char* valueString = NULL; 
	 
	
	//当前待解析JSON字符串的类型，是对象还是数组 
	int currentJsonType = -1;
	
	//保存一个当前传入的起始解析位置 
	int currentStartIndex = *index;
	
	//当前待解析JSON字符串的根结点指针 
	MyJson* thisRoot = NULL; 
	
	for(int i = *index;i < strlen(json);i++)
	{
		if(index && i > currentStartIndex)
		{
			//printf("===>[(*index):%d,%c]\n",(*index),json[*index]);
			(*index)++; 
		}
		if(i == currentStartIndex && json[currentStartIndex] == '{')
		{
			//此时说明当前json字符串是对象 
			thisRoot = createObject(keyString);
			addNodeToParent(parent,thisRoot);
			currentJsonType = 6;
			if(i + 1 < strlen(json) && json[i + 1] == '}')
			{
				//说明此时是对象内容为空，直接返回即可
				*index = i + 1; 
				break; 
			}
		}
		else if(i == currentStartIndex && json[currentStartIndex] == '[')
		{
			//此时说明当前json字符串是数组 
			thisRoot = createArray(keyString);
			addNodeToParent(parent,thisRoot);
			currentJsonType = 5;
			if(i + 1 < strlen(json) && json[i + 1] == ']')
			{
				//说明此时是数组内容为空，直接返回即可
				*index = i + 1; 
				break; 
			}
		}
		else if(i > currentStartIndex)
		{
			//开始分析json字符串
			if(currentJsonType == 6)
			{
				if(loc[0] == 0)
				{
					//未找到键的起始位置
					if(json[i] == ' ' || json[i] == '\n' || json[i] == '\t')
					{
						//puts("\n找键时:跳过");
						continue;
					}
					else if(json[i] == '\"' && json[i - 1] != '\\')
					{
						loc[0] = i;
						//printf("\n====>>>keyString:");	
					}
					else
					{
						puts("\n[PraseError]JSON找到键起始位置前，语法错误！");
						freeJson(&thisRoot);
						break;	
					} 
				}
				else
				{
					//已找到键的起始位置
					if(loc[1] == 0)
					{
						//未找到键的结束位置
						if(json[i] == '\"' && json[i - 1] != '\\')
						{
							loc[1] = i;
							//puts("\n已经找到键的起止范围!");
						}
						else
						{
							//printf("%c",json[i]);
						}
					}
					else
					{
						//已经找到键的结束位置
						if(loc[2] == 0)
						{
							//未找到键值之间的冒号:位置
							if(json[i] == ' ' || json[i] == '\n' || json[i] == '\t')
							{
								//puts("\n找冒号时:跳过");
								continue;
							}
							else if(json[i] == ':')
							{
								loc[2] = i;
								//puts("\n已找到键值冒号：位置!");
							}
							else
							{
								puts("\n[PraseError]JSON找到键值冒号前，语法错误！");
								freeJson(&thisRoot);
								break;
							}
						}
						else
						{
							//已找到键值之间的冒号:位置
							if(loc[3] == 0)
							{
								//未找到值的起始位置
								if(json[i] == ' ' || json[i] == '\n' || json[i] == '\t')
								{
									//puts("\n找值时:跳过");
									continue;	
								}
								else if((json[i] >= '0' && json[i] <= '9') || json[i] == '-' || json[i] == '+')
								{
									//预估找到的值类型是数值-int 
									//puts("\n预计值是数值-int!"); 
									//puts("\n===>>>value:");
									loc[3] = i;
									loc[6] = 0; //把数值类型默认当作int解析，在读入字符途中会执行修正 
									valueString = (char*) malloc(sizeof(char) * 2);
									valueString[0] = json[i];
									valueString[1] = '\0';
								}
								else if(json[i] == 'f' || json[i] == 't')
								{
									//预估找到的值类型是布尔-bool
									//puts("\n预计值是布尔-bool");
									//puts("\n===>>>value:");
									loc[3] = i;
									loc[6] = 2;
									valueString = (char*) malloc(sizeof(char) * 2);
									valueString[0] = json[i];
									valueString[1] = '\0';
								}
								else if(json[i] == 'n')
								{
									//预估找到的值类型是null
									//puts("\n预估值是空-null");
									//puts("\n===>>>value:");
									loc[3] = i;
									loc[6] = 3;
									valueString = (char*) malloc(sizeof(char) * 2);
									valueString[0] = json[i];
									valueString[1] = '\0'; 
								}
								else if(json[i] == '\"')
								{
									//预估找到的值类型是字符串-string
									//puts("\n预计值是字符串-string"); 
									//puts("\n===>>>value:");
									loc[3] = i;
									loc[6] = 4;
								}
								else if(json[i] == '[')
								{
									//预估找到的值类型是数组-array
									//puts("\n预计值是数组-array");
									//puts("\n===>>>value:");
									loc[3] = i;
									loc[6] = 5;
									
									int keyStringlen = loc[1] - loc[0];
									char* keyString = (char*) malloc(sizeof(char) * keyStringlen);
									//char keyString[keyStringlen];
									int k = 0;
									for(int j = loc[0] + 1;j < loc[1];j++)
									{
										keyString[k++] = json[j];
									}
									keyString[k] = '\0';
									
									if(!prase(thisRoot,json,keyString,index))
									{
										puts("\n[PraseError]JSON子数组结构中有错误，语法错误！");
										freeJson(&thisRoot);
										break;
									}
									else
									{
										//printf("当前位置json[%d] = %c\n",i,json[i]);
										
										loc[4] = *index;
										i = *index;
									}
								}
								else if(json[i] == '{')
								{
									//预估找到的值类型是对象-object
									//puts("\n预计值是对象-object");
									//puts("\n===>>>value:");
									loc[3] = i;
									loc[6] = 6;
									
									int keyStringlen = loc[1] - loc[0];
									char* keyString = (char*) malloc(sizeof(char) * keyStringlen);
									//char keyString[keyStringlen];
									int k = 0;
									for(int j = loc[0] + 1;j < loc[1];j++)
									{
										keyString[k++] = json[j];
									}
									keyString[k] = '\0';
									
									
									if(!prase(thisRoot,json,keyString,index))
									{
										puts("\n[PraseError]JSON子对象结构中有错误，语法错误！");
										freeJson(&thisRoot);
										break;
									}
									else
									{
										//printf("当前位置json[%d] = %c\n",i,json[i]);
										
										loc[4] = *index;
										i = *index;
									} 
								}
								else
								{
									//puts("\n[PraseError]JSON找到值起始前，语法错误！");
									freeJson(&thisRoot);
									break; 
								}
							}
							else
							{
								//已找到值的起始位置
								if(loc[4] == 0)
								{
									//未找到值的结束位置
									if(loc[6] == 0)
									{
										if((json[i] < '0' || json[i] > '9') && json[i] != '.' && json[i] != ',' && json[i] != ']' && json[i] != '}' && json[i] != '\n' && json[i] != ' ' && json[i] != '\t')
										{
											puts("\n[PraseError]解析整数出错，发现除数字之外的字符但不是小数点，语法错误！");
											freeJson(&thisRoot);
											free(valueString);
											valueString = NULL; 
											break;
										}
										if(json[i] == ',' || json[i] == '}' || json[i] == ']')
										{
											loc[4] = i - 1;
											i = i - 1;
											index ? (*index)-- : 0; 
											continue;
										}
										valueString = (char*) realloc(valueString,sizeof(char) * (strlen(valueString) + 2));
												
										int len = strlen(valueString);
										valueString[len] = json[i];
										valueString[len + 1] = '\0';
										
										if(valueString[len] == '.')
										{
											//默认把数值类型当作int解析，但是发现有小数点，则更正为double类型 
											loc[6] = 1;
										}
									}
									else if(loc[6] == 1)
									{
										if((json[i] < '0' || json[i] > '9') && json[i] != ',')
										{
											puts("\n[PraseError]解析小数数值出错，发现非数字字符，语法错误！");
											freeJson(&thisRoot);
											free(valueString);
											valueString = NULL; 
											break;
										}
										if(json[i] == ',' || json[i] == '}' || json[i] == ']')
										{
											loc[4] = i - 1;
											i = i - 1;
											index ? (*index)-- : 0; 
											continue;
										}
										valueString = (char*) realloc(valueString,sizeof(char) * (strlen(valueString) + 2));
												
										int len = strlen(valueString);
										valueString[len] = json[i];
										valueString[len + 1] = '\0';
									}
									else if(loc[6] == 2)
									{
										if(valueString[0] == 'f')
										{
											if(strlen(valueString) < 5)
											{
												valueString = (char*) realloc(valueString,sizeof(char) * (strlen(valueString) + 2));
												
												int len = strlen(valueString);
												valueString[len] = json[i];
												valueString[len + 1] = '\0';
												
												if(strlen(valueString) == 5)
												{
													//puts(valueString);
													
													if(strcmp(valueString,"false") == 0)
													{
														loc[4] = i;
													}
													else
													{
														puts("\n[PraseError]布尔false拼写错误，语法错误！");
														freeJson(&thisRoot);
														break;
													}
												}
											}
										}
										else if(valueString[0] == 't')
										{
											if(strlen(valueString) < 4)
											{
												valueString = (char*) realloc(valueString,sizeof(char) * (strlen(valueString) + 2) );
												int len = strlen(valueString);
												valueString[len] = json[i];
												valueString[len + 1] = '\0';
												
												if(strlen(valueString) == 4)
												{
													//puts(valueString);
													
													if(strcmp(valueString,"true") == 0)
													{
														loc[4] = i;
													}
													else
													{
														puts("\n[PraseError]布尔true拼写错误，语法错误！");
														break;
													}
												}
											}
										}
									}
									else if(loc[6] == 3)
									{
										if(strlen(valueString) < 4)
										{
											valueString = (char*) realloc(valueString,sizeof(char) * (strlen(valueString) + 2) );
											int len = strlen(valueString);
											valueString[len] = json[i];
											valueString[len + 1] = '\0';
											
											if(strlen(valueString) == 4)
											{
												//puts(valueString);
												
												if(strcmp(valueString,"null") == 0)
												{
													loc[4] = i;
												}
												else
												{
													puts("\n[PraseError]空null拼写错误，语法错误！");
													freeJson(&thisRoot);
													break;
												}	
											}
										}	
									}
									else if(loc[6] == 4)
									{
										if(json[i] == '\"' && json[i - 1] != '\\')
										{
											loc[4] = i;
											//puts("\n已找到字符串值结束位置!");
										}
									}
									else if(loc[6] == 5)
									{
										//不用管这里已经进入了递归子函数中解析 
									}
									else if(loc[6] == 6)
									{
										//不用管这里已经进入了递归子函数中解析 
									}
								} 
								else
								{
									//已找到值的结束位置
									if(loc[5] == 0)
									{
										//未找到键值对间分割标志：,或}
										if(loc[6] == 0)
										{
											if(json[i] == ' ' || json[i] == '\n' || json[i] == '\t')
											{
												//puts("\n找键值对间分割标志时:跳过");
												continue;	
											}
											else if(json[i] == ',' || json[i] == '}')
											{
												int keyStringlen = loc[1] - loc[0];
												char* keyString = (char*) malloc(sizeof(char) * keyStringlen);
												//char keyString[keyStringlen];
												int k = 0;
												for(int j = loc[0] + 1;j < loc[1];j++)
												{
													keyString[k++] = json[j];
												}
												keyString[k] = '\0';
												
												//printf("整数：%s\n",valueString);
												//puts(valueString);
												
												addInt(thisRoot,keyString,atoi(valueString));
												
												valueString = NULL;
												loc[0] = 0;
												loc[1] = 0;
												loc[2] = 0;
												loc[3] = 0;
												loc[4] = 0;
												loc[5] = 0;
												loc[6] = -1;
												if(json[i] == '}')
												{
													break;
												}
											}
											else
											{
												puts("\n[PraseError]JSON找到键值对间分割标志前，语法错误！");
												freeJson(&thisRoot);
												break;
											}
										}
										else if(loc[6] == 1)
										{
											if(json[i] == ' ' || json[i] == '\n' || json[i] == '\t')
											{
												//puts("\n找键值对间分割标志时:跳过");
												continue;	
											}
											else if(json[i] == ',' || json[i] == '}')
											{
												int keyStringlen = loc[1] - loc[0];
												char* keyString = (char*) malloc(sizeof(char) * keyStringlen);
												//char keyString[keyStringlen];
												int k = 0;
												for(int j = loc[0] + 1;j < loc[1];j++)
												{
													keyString[k++] = json[j];
												}
												keyString[k] = '\0';
												
												//printf("小数：%s\n",valueString);
												//puts(valueString);
												
												addDouble(thisRoot,keyString,atof(valueString));
												
												valueString = NULL;
												loc[0] = 0;
												loc[1] = 0;
												loc[2] = 0;
												loc[3] = 0;
												loc[4] = 0;
												loc[5] = 0;
												loc[6] = -1;
												if(json[i] == '}')
												{
													break;
												}
											}
											else
											{
												puts("\n[PraseError]JSON找到键值对间分割标志前，语法错误！");
												freeJson(&thisRoot);
												break;
											}
										}
										else if(loc[6] == 2)
										{
											if(json[i] == ' ' || json[i] == '\n' || json[i] == '\t')
											{
												//puts("\n找键值对间分割标志时:跳过");
												continue;	
											}
											else if(json[i] == ',' || json[i] == '}')
											{
												int keyStringlen = loc[1] - loc[0];
												char* keyString = (char*) malloc(sizeof(char) * keyStringlen);
												//char keyString[keyStringlen];
												int k = 0;
												for(int j = loc[0] + 1;j < loc[1];j++)
												{
													keyString[k++] = json[j];
												}
												keyString[k] = '\0';
												
												if(strcmp(valueString,"false") == 0)
												{
													addBool(thisRoot,keyString,myFalse);
												}
												else
												{
													addBool(thisRoot,keyString,myTrue);
												}
												
												valueString = NULL;
												loc[0] = 0;
												loc[1] = 0;
												loc[2] = 0;
												loc[3] = 0;
												loc[4] = 0;
												loc[5] = 0;
												loc[6] = -1;
												if(json[i] == '}')
												{
													break;
												}	
											}
											else
											{
												puts("\n[PraseError]JSON找到键值对间分割标志前，语法错误！");
												freeJson(&thisRoot);
												break;
											}
										} 
										else if(loc[6] == 3)
										{
											if(json[i] == ' ' || json[i] == '\n' || json[i] == '\t')
											{
												//puts("\n找键值对间分割标志时:跳过");
												continue;	
											}
											else if(json[i] == ',' || json[i] == '}')
											{
												int keyStringlen = loc[1] - loc[0];
												
												char* keyString = (char*) malloc(sizeof(char) * keyStringlen); 
												//char keyString[keyStringlen];
												
												int k = 0;
												for(int j = loc[0] + 1;j < loc[1];j++)
												{
													keyString[k++] = json[j];
												}
												keyString[k] = '\0';
												
												addNull(thisRoot,keyString);
												
												valueString = NULL;
												loc[0] = 0;
												loc[1] = 0;
												loc[2] = 0;
												loc[3] = 0;
												loc[4] = 0;
												loc[5] = 0;
												loc[6] = -1;
												if(json[i] == '}')
												{
													break;
												}
											}
											else
											{
												puts("\n[PraseError]JSON找到键值对间分割标志前，语法错误！");
												freeJson(&thisRoot);
												break;	
											} 
										} 
										else if(loc[6] == 4)
										{
											if(json[i] == ' ' || json[i] == '\n' || json[i] == '\t')
											{
												//puts("\n找键值对间分割标志时:跳过");
												continue;	
											}
											else if(json[i] == ',' || json[i] == '}')
											{
												int keyStringlen = loc[1] - loc[0];
												
												char* keyString = (char*) malloc(sizeof(char) * keyStringlen); 
												//char keyString[keyStringlen];
												
												int k = 0;
												for(int j = loc[0] + 1;j < loc[1];j++)
												{
													keyString[k++] = json[j];
												}
												keyString[k] = '\0';
												
												int valueStringlen = loc[4] - loc[3];
												//char valueString[valueStringlen];
												valueString = (char*) malloc(sizeof(char) * valueStringlen); 
												k = 0;
												for(int j = loc[3] + 1;j < loc[4];j++)
												{
													valueString[k++] = json[j];
												}
												valueString[k] = '\0';
												
												addString(thisRoot,keyString,valueString); 
												
											
												//puts(valueString);
												
												valueString = NULL;
												loc[0] = 0;
												loc[1] = 0;
												loc[2] = 0;
												loc[3] = 0;
												loc[4] = 0;
												loc[5] = 0;
												loc[6] = -1;
												if(json[i] == '}')
												{
													break;
												}
											}
											else
											{
												puts("\n[PraseError]JSON找到键值对间分割标志前，语法错误！");
												freeJson(&thisRoot);
												break;
											}
										}
										else if(loc[6] == 5)
										{
											if(json[i] == ' ' || json[i] == '\n' || json[i] == '\t')
											{
												//puts("\n找键值对间分割标志时:跳过");
												continue;	
											}
											else if(json[i] == ',' || json[i] == '}')
											{
												valueString = NULL;
												loc[0] = 0;
												loc[1] = 0;
												loc[2] = 0;
												loc[3] = 0;
												loc[4] = 0;
												loc[5] = 0;
												loc[6] = -1;
												if(json[i] == '}')
												{
													break;
												}
											}
											else
											{
												puts("\n[PraseError]JSON找到键值对间分割标志前，语法错误！");
												freeJson(&thisRoot);
												break;
											}
										} 
										else if(loc[6] == 6)
										{
											if(json[i] == ' ' || json[i] == '\n' || json[i] == '\t')
											{
												//puts("\n找键值对间分割标志时:跳过");
												continue;	
											}
											else if(json[i] == ',' || json[i] == '}')
											{
												valueString = NULL;
												loc[0] = 0;
												loc[1] = 0;
												loc[2] = 0;
												loc[3] = 0;
												loc[4] = 0;
												loc[5] = 0;
												loc[6] = -1;
												if(json[i] == '}')
												{
													break;
												}
											}
											else
											{
												puts("\n[PraseError]JSON找到键值对间分割标志前，语法错误！");
												freeJson(&thisRoot);
												break;
											}
										} 
									}
									else
									{
										//已找到键值分割标志：,或} 	
									}	
								} 
							} 
						} 
					} 
				} 
			}
			else if(currentJsonType == 5)
			{
				loc[0] = 0;
				loc[1] = 0;
				loc[2] = 0; 

				if(loc[3] == 0)
				{
					//未找到值的起始位置
					if(json[i] == ' ' || json[i] == '\n' || json[i] == '\t')
					{
						//puts("\n数组中，找值时:跳过");
						continue;	
					}
					else if((json[i] >= '0' && json[i] <= '9') || json[i] == '-' || json[i] == '+')
					{
						//预估找到的值类型是数值-int 
						//puts("\n预计值是数值-int!"); 
						//puts("\n===>>>value:");
						loc[3] = i;
						loc[6] = 0; //把数值类型默认当作int解析，在读入字符途中会执行修正 
						valueString = (char*) malloc(sizeof(char) * 2);
						valueString[0] = json[i];
						valueString[1] = '\0';
					}
					else if(json[i] == 'f' || json[i] == 't')
					{
						//预估找到的值类型是布尔-bool
						//puts("\n预计值是布尔-bool");
						//puts("\n===>>>value:");
						loc[3] = i;
						loc[6] = 2;
						valueString = (char*) malloc(sizeof(char) * 2);
						valueString[0] = json[i];
						valueString[1] = '\0';
					}
					else if(json[i] == 'n')
					{
						//预估找到的值类型是null
						//puts("\n预估值是空-null");
						//puts("\n===>>>value:");
						loc[3] = i;
						loc[6] = 3;
						valueString = (char*) malloc(sizeof(char) * 2);
						valueString[0] = json[i];
						valueString[1] = '\0'; 
					}
					else if(json[i] == '\"')
					{
						//预估找到的值类型是字符串-string
						//puts("\n预计值是字符串-string"); 
						//puts("\n===>>>value:");
						loc[3] = i;
						loc[6] = 4;
					}
					else if(json[i] == '[')
					{
						//预估找到的值类型是数组-array
						//puts("\n预计值是数组-array");
						//puts("\n===>>>value:");
						loc[3] = i;
						loc[6] = 5;
						
						if(!prase(thisRoot,json,NULL,index))
						{
							puts("\n[PraseError]JSON子数组结构中有错误，语法错误！");
							freeJson(&thisRoot);
							break;
						}
						else
						{
							//printf("当前位置json[%d] = %c\n",i,json[i]);
							
							loc[4] = *index;
							i = *index;
							//puts(&json[i]); 
						}
					}
					else if(json[i] == '{')
					{
						//预估找到的值类型是对象-object
						//puts("\n预计值是对象-object");
						//puts("\n===>>>value:");
						loc[3] = i;
						loc[6] = 6;
						
						if(!prase(thisRoot,json,NULL,index))
						{
							puts("\n[PraseError]JSON子对象结构中有错误，语法错误！");
							freeJson(&thisRoot);
							break;
						}
						else
						{
							//printf("当前位置json[%d] = %c\n",i,json[i]);
							
							loc[4] = *index;
							i = *index;
						} 
					}
					else
					{
						puts("\n[PraseError]JSON找到值起始前，语法错误！");
						freeJson(&thisRoot);
						break; 
					} 
				}
				else
				{
					//已找到值的起始位置
					if(loc[4] == 0)
					{
						//未找到值的结束位置
						if(loc[6] == 0)
						{
							if((json[i] < '0' || json[i] > '9') && json[i] != '.' && json[i] != ',' && json[i] != ']' && json[i] != '\n' && json[i] != ' ' && json[i] != '\t')
							{
								puts("\n[PraseError]解析整数出错，发现除数字之外的字符但不是小数点，语法错误！");
								freeJson(&thisRoot);
								free(valueString);
								valueString = NULL; 
								break;
							}
							if(json[i] == ',' || json[i] == ']')
							{
								loc[4] = i - 1;
								i = i - 1;
								index ? (*index)-- : 0; 
								continue;
							}
							
							valueString = (char*) realloc(valueString,sizeof(char) * (strlen(valueString) + 2));
									
							int len = strlen(valueString);
							valueString[len] = json[i];
							valueString[len + 1] = '\0';
							
							if(valueString[len] == '.')
							{
								//默认把数值类型当作int解析，但是发现有小数点，则更正为double类型 
								loc[6] = 1;
							}
						}
						else if(loc[6] == 1)
						{
							if((json[i] < '0' || json[i] > '9') && json[i] != '.' && json[i] != ',' && json[i] != ']' && json[i] != '\n' && json[i] != ' ' && json[i] != '\t')
							{
								puts("\n[PraseError]解析小数数值出错，发现非数字字符，语法错误！");
								freeJson(&thisRoot);
								free(valueString);
								valueString = NULL; 
								break;
							}
							if(json[i] == ',' || json[i] == ']')
							{
								loc[4] = i - 1;
								i = i - 1;
								index ? (*index)-- : 0; 
								continue;
							}
							valueString = (char*) realloc(valueString,sizeof(char) * (strlen(valueString) + 2));
									
							int len = strlen(valueString);
							valueString[len] = json[i];
							valueString[len + 1] = '\0';
						}
						else if(loc[6] == 2)
						{
							if(valueString[0] == 'f')
							{
								if(strlen(valueString) < 5)
								{
									valueString = (char*) realloc(valueString,sizeof(char) * (strlen(valueString) + 2));
									
									int len = strlen(valueString);
									valueString[len] = json[i];
									valueString[len + 1] = '\0';
									
									if(strlen(valueString) == 5)
									{
										//puts(valueString);
										
										if(strcmp(valueString,"false") == 0)
										{
											loc[4] = i;
										}
										else
										{
											puts("\n[PraseError]布尔false拼写错误，语法错误！");
											freeJson(&thisRoot);
											break;
										}
									}
								}
							}
							else if(valueString[0] == 't')
							{
								if(strlen(valueString) < 4)
								{
									valueString = (char*) realloc(valueString,sizeof(char) * (strlen(valueString) + 2) );
									int len = strlen(valueString);
									valueString[len] = json[i];
									valueString[len + 1] = '\0';
									
									if(strlen(valueString) == 4)
									{
										//puts(valueString);
										
										if(strcmp(valueString,"true") == 0)
										{
											loc[4] = i;
										}
										else
										{
											puts("\n[PraseError]布尔true拼写错误，语法错误！");
											break;
										}
									}
								}
							}
						}
						else if(loc[6] == 3)
						{
							if(strlen(valueString) < 4)
							{
								valueString = (char*) realloc(valueString,sizeof(char) * (strlen(valueString) + 2) );
								int len = strlen(valueString);
								valueString[len] = json[i];
								valueString[len + 1] = '\0';
								
								if(strlen(valueString) == 4)
								{
									//puts(valueString);
									
									if(strcmp(valueString,"null") == 0)
									{
										loc[4] = i;
									}
									else
									{
										puts("\n[PraseError]空null拼写错误，语法错误！");
										freeJson(&thisRoot);
										break;
									}	
								}
							}	
						}
						else if(loc[6] == 4)
						{
							if(json[i] == '\"' && json[i - 1] != '\\')
							{
								loc[4] = i;
								//puts("\n已找到字符串值结束位置!");
							}
						}
						else if(loc[6] == 5)
						{
							//不用管这里已经进入了递归子函数中解析 
						}
						else if(loc[6] == 6)
						{
							//不用管这里已经进入了递归子函数中解析 
						}
					} 
					else
					{
						//已找到值的结束位置
						if(loc[5] == 0)
						{
							//未找到键值对间分割标志：,或}
							if(loc[6] == 0)
							{
								if(json[i] == ' ' || json[i] == '\n' || json[i] == '\t')
								{
									//puts("\n找键值对间分割标志时:跳过");
									continue;	
								}
								else if(json[i] == ',' || json[i] == ']')
								{
									//printf("整数：%s\n",valueString);
									//puts(valueString);
									
									addInt(thisRoot,NULL,atoi(valueString));
									
									valueString = NULL;
									loc[0] = 0;
									loc[1] = 0;
									loc[2] = 0;
									loc[3] = 0;
									loc[4] = 0;
									loc[5] = 0;
									loc[6] = -1;
									if(json[i] == ']')
									{
										break;
									}
								}
								else
								{
									puts("\n[PraseError][In Arrray][Int]JSON找到键值对间分割标志前，语法错误！");
									freeJson(&thisRoot);
									break;
								}
							}
							else if(loc[6] == 1)
							{
								if(json[i] == ' ' || json[i] == '\n' || json[i] == '\t')
								{
									//puts("\n找键值对间分割标志时:跳过");
									continue;	
								}
								else if(json[i] == ',' || json[i] == ']')
								{	
									//printf("小数：%s\n",valueString);
									//puts(valueString);
									
									addDouble(thisRoot,NULL,atof(valueString));
									
									valueString = NULL;
									loc[0] = 0;
									loc[1] = 0;
									loc[2] = 0;
									loc[3] = 0;
									loc[4] = 0;
									loc[5] = 0;
									loc[6] = -1;
									if(json[i] == ']')
									{
										break;
									}
								}
								else
								{
									puts("\n[PraseError][In Arrray][Double]JSON找到键值对间分割标志前，语法错误！");
									freeJson(&thisRoot);
									break;
								}
							}
							else if(loc[6] == 2)
							{
								if(json[i] == ' ' || json[i] == '\n' || json[i] == '\t')
								{
									//puts("\n找键值对间分割标志时:跳过");
									continue;	
								}
								else if(json[i] == ',' || json[i] == ']')
								{
									if(strcmp(valueString,"false") == 0)
									{
										addBool(thisRoot,NULL,myFalse);
									}
									else
									{
										addBool(thisRoot,NULL,myTrue);
									}
									
									valueString = NULL;
									loc[0] = 0;
									loc[1] = 0;
									loc[2] = 0;
									loc[3] = 0;
									loc[4] = 0;
									loc[5] = 0;
									loc[6] = -1;
									if(json[i] == ']')
									{
										break;
									}
								}
								else
								{
									puts("\n[PraseError][In Arrray][Bool]JSON找到键值对间分割标志前，语法错误！");
									freeJson(&thisRoot);
									break;
								}
							} 
							else if(loc[6] == 3)
							{
								if(json[i] == ' ' || json[i] == '\n' || json[i] == '\t')
								{
									//puts("\n找键值对间分割标志时:跳过");
									continue;	
								}
								else if(json[i] == ',' || json[i] == ']')
								{
									addNull(thisRoot,NULL);
									
									valueString = NULL;
									loc[0] = 0;
									loc[1] = 0;
									loc[2] = 0;
									loc[3] = 0;
									loc[4] = 0;
									loc[5] = 0;
									loc[6] = -1;
									if(json[i] == ']')
									{
										break;
									}
								}
								else
								{
									puts("\n[PraseError][In Arrray][Null]JSON找到键值对间分割标志前，语法错误！");
									freeJson(&thisRoot);
									break;	
								} 
							} 
							else if(loc[6] == 4)
							{
								if(json[i] == ' ' || json[i] == '\n' || json[i] == '\t')
								{
									//puts("\n找键值对间分割标志时:跳过");
									continue;	
								}
								else if(json[i] == ',' || json[i] == ']')
								{	
									int valueStringlen = loc[4] - loc[3];
									//char valueString[valueStringlen];
									valueString = (char*) malloc(sizeof(char) * valueStringlen); 
									int k = 0;
									for(int j = loc[3] + 1;j < loc[4];j++)
									{
										valueString[k++] = json[j];
									}
									valueString[k] = '\0';
									
									addString(thisRoot,NULL,valueString); 
									
									//puts(valueString);
									
									valueString = NULL;
									loc[0] = 0;
									loc[1] = 0;
									loc[2] = 0;
									loc[3] = 0;
									loc[4] = 0;
									loc[5] = 0;
									loc[6] = -1;
									if(json[i] == ']')
									{
										break;
									}
								}
								else
								{
									puts("\n[PraseError][In Arrray][String]JSON找到键值对间分割标志前，语法错误！");
									puts(&json[i]);
									freeJson(&thisRoot);
									break;
								}
							}
							else if(loc[6] == 5)
							{
								if(json[i] == ' ' || json[i] == '\n' || json[i] == '\t')
								{
									//puts("\n找键值对间分割标志时:跳过");
									continue;	
								}
								else if(json[i] == ',' || json[i] == ']')
								{
									valueString = NULL;
									loc[0] = 0;
									loc[1] = 0;
									loc[2] = 0;
									loc[3] = 0;
									loc[4] = 0;
									loc[5] = 0;
									loc[6] = -1;
									if(json[i] == ']')
									{
										break;
									}
								}
								else
								{
									puts("\n[PraseError][In Arrray][Array]JSON找到键值对间分割标志前，语法错误！");
									freeJson(&thisRoot);
									break;
								}
							} 
							else if(loc[6] == 6)
							{
								if(json[i] == ' ' || json[i] == '\n' || json[i] == '\t')
								{
									//puts("\n找键值对间分割标志时:跳过");
									continue;	
								}
								else if(json[i] == ',' || json[i] == ']')
								{
									valueString = NULL;
									loc[0] = 0;
									loc[1] = 0;
									loc[2] = 0;
									loc[3] = 0;
									loc[4] = 0;
									loc[5] = 0;
									loc[6] = -1;
									if(json[i] == ']')
									{
										break;
									}
								}
								else
								{
									puts("\n[PraseError][In Arrray][Object]JSON找到键值对间分割标志前，语法错误！");
									freeJson(&thisRoot);
									break;
								}
							}
						} 
						else
						{
							////已找到键值对间分割标志：,或}
						}
					} 
				}
			}
		}
	}
	
	return thisRoot; 
}     

//从给定的JSON中得到key结点指针返回
MyJson* getValue(MyJson* parent,const char* keyString)
{
	if(!parent || !keyString)
	{
		return NULL;
	}
	Keys* keys = getKeys(parent);
	for(int i = 0;i < keys->keysLength;i++)
	{
		if(strcmp((keys->keys)[i]->keyName,keyString) == 0)
		{
			return (keys->keys)[i];
		}
	}
	
	return NULL;	
}             

//从给定的JSON父结点中返回所有key结点指针 
Keys* getKeys(MyJson* parent)
{
	if(!parent)
	{
		return NULL;
	}
	if(parent->type != 5 && parent->type != 6)
	{
		return NULL;
	}
	
	MyJson* t = parent->child;
	
	MyJson** results = (MyJson**) malloc(sizeof(MyJson*) * 0);
	int len = 0;
	while(1)
	{
		if(!t)
		{
			break;
		}
		
		results = (MyJson**) realloc(results,sizeof(MyJson*) * (++len));
		results[len - 1] = t;
		t = t->next;
	}
	
	Keys* keys = (Keys*) malloc(sizeof(Keys) * 1);
	
	keys->keys = results;
	keys->keysLength = len;
	
	
	return !len ? NULL : keys;	
}        
                            
/********************************由JSON字符串结构解析成C语言可用数据类型**********************************/

/*****************************************辅助函数*******************************************************/
myBool getStringOtherSpace(char** jsonStringPointer, int* jsonStringSpaceLengthPointer, int toUsingSpaceLength)
{
	int currentArrayIndex = strlen(*jsonStringPointer);
	
	if(toUsingSpaceLength > (*jsonStringSpaceLengthPointer - currentArrayIndex))
	{
		return myFalse;
	}
	
	return myTrue;
}

myBool strcatString(char** jsonStringPointer, int* jsonStringSpaceLengthPointer, char* keyValueString,int totalLength)
{
	if(getStringOtherSpace(jsonStringPointer, jsonStringSpaceLengthPointer, totalLength))
	{
		strcat(*jsonStringPointer,keyValueString);	
	}
	else
	{
		char* bakJsonString = *jsonStringPointer;
		*jsonStringPointer = (char*) realloc(*jsonStringPointer, *jsonStringSpaceLengthPointer + 128);
		
		if(*jsonStringPointer == NULL)
		{
			*jsonStringPointer = bakJsonString;
			return myFalse;
		}
		
		*jsonStringSpaceLengthPointer = *jsonStringSpaceLengthPointer + 128; 
		strcat(*jsonStringPointer,keyValueString);
	}
}
/*****************************************辅助函数*******************************************************/
